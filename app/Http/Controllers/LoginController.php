<?php

namespace App\Http\Controllers;

use GrahamCampbell\ResultType\Success;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index()
    {
        // dd(bcrypt('siswa'));
        return view('login.login');
    }

    public function proseslogin(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        $cek = $request->only(['username', 'password']);
        if (Auth::attempt($cek)) {
            return redirect()->route('dashboard')->with('Success', "berhasil login.");
        } else {
            return back()->with('error', "Gagal login.");
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login')->with('succes', "berhasil keluar");
    }
}
