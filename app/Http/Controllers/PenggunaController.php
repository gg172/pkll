<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Level;

class PenggunaController extends Controller
{
    // public function index()
    // {
    //     $data = User::all();
    //     // dd($data);
    //     return view('pengguna.pengguna', ['pengguna' => $data]);
    // }

    // public function tambah()
    // {
    //     // dd(Level::all());
    //     $level = Level::all();
    //     return view('pengguna.tambah', ['level' => $level]);
    // }

    // public function simpan(Request $request)
    // {
    //     $id_user = new User;
    //     $id_user = $id_user->max('id_user') + 1;

    //     User::create([
    //         'id_user' => $id_user,
    //         'nama' => $request->nama,
    //         'username' => $request->username,
    //         'password' => $request->password,
    //         'level_user' => $request->level_user,
    //     ]);
    //     return redirect()->route('user');
    // }

    // public function edit($id_user)
    // {
    //     // dd($id_user);
    //     $data = User::find($id_user);
    //     $level = Level::all();
    //     return view('pengguna.edit', ['data' => $data]);
    // }

    // public function update(Request $request)
    // {
    //     $data = User::find($request->id_user);
    //     // $id_buku = $id_buku->max('id_buku') + 1;
    //     $id_user = $data->id_user;


    //     $data->nama = $request->nama;
    //     $data->username = $request->username;
    //     $data->level_user = $request->level_user;
    //     $data->password = $request->password;

    //     $data->save();
    //     return redirect()->route('user');
    // }

    // public function hapus($id)
    // {
    //     //dd($id_buku);
    //     $data = User::findorfail($id);
    //     $path = public_path() . '/storage/pengguna/' . $id;
    //     $df = $data->foto;
    //     if (!is_null($df)) {
    //         $path = $path . '/' . $df;

    //         if (file_exists($path)) {
    //             unlink($path);
    //         }
    //     }
    //     $data->delete();
    //     return redirect(route('user'));
    // }
    public function index()
    {
        $data = User::all();
        // dd($buku);
        return view('pengguna.pengguna', ['pengguna' => $data]);
    }

    public function tambah()
    {
        // dd(Level::all());
        $level = Level::all();
        return view('pengguna.tambah', ['level' => $level]);
    }

    public function simpan(Request $request)
    {
        $id_user = new User;
        $id_user = $id_user->max('id_user') + 1;
        $foto = null;
        if ($request->hasFile('filename')) {
            $path = public_path() . '/storage/pengguna/' . $id_user;
            if (!is_dir($path)) {
                mkdir($path, 0755, true);
            }
            foreach ($request->file('filename') as $image) {
                $name = $image->getClientOriginalName();
                $image->move($path, $name);
                $foto = $name;
            }
        }

        $password = bcrypt($request->password);
        User::create([
            'id_user' => $id_user,
            'nama' => $request->nama,
            'username' => $request->username,
            'password' => $password,
            'level_user' => $request->level_user,
            'foto' => $foto
        ]);
        return redirect()->route('user');
    }

    public function edit($id_user)
    {
        // dd($id);
        $data = User::find($id_user);
        $level = Level::all();
        return view('pengguna.edit', ['data' => $data]);
    }

    public function update(Request $request)
    {
        $data = User::find($request->id_user);
        // $id_buku = $id_buku->max('id_buku') + 1;
        $id_user = $data->id_user;
        $foto = null;
        if ($request->hasFile('filename')) {

            $pathori = public_path() . '/storage/pengguna/' . $id_user;
            $path = public_path() . '/storage/pengguna/' . $id_user;
            $df = $data->foto;
            if (!is_null($df)) {
                $path = $path . '/' . $df;

                if (file_exists($path)) {
                    unlink($path);
                }
            }
            if (!is_dir($pathori)) {
                mkdir($pathori, 0755, true);
            }
            foreach ($request->file('filename') as $image) {
                $name = $image->getClientOriginalName();
                $image->move($pathori, $name);
                $foto = $name;
            }
            $data->foto = $foto;
        }

        $data->nama = $request->nama;
        $data->username = $request->username;
        $data->level_user = $request->level_user;
        if ($request->password) {
            $password = bcrypt($request->password);
            $data->password = $password;
        }
        $data->save();
        return redirect()->route('user');
    }

    public function hapus($id_user)
    {
        //dd($id_buku);
        $data = User::findorfail($id_user);
        $path = public_path() . '/storage/pengguna/' . $id_user;
        $df = $data->foto;
        if (!is_null($df)) {
            $path = $path . '/' . $df;

            if (file_exists($path)) {
                unlink($path);
            }
        }
        $data->delete();
        return redirect(route('user'));
    }
}
