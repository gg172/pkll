<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home.home');
});

Route::get('login', 'App\Http\Controllers\LoginController@index')->name('login');
Route::get('logout', 'App\Http\Controllers\LoginController@logout')->name('logout');
Route::post('proseslogin', 'App\Http\Controllers\LoginController@proseslogin')->name('proseslogin');


// Route::get('user', 'App\Http\Controllers\PenggunaController@index')->name('user');
// Route::get('user/tambah', 'App\Http\Controllers\PenggunaController@tambah')->name('user.tambah');
// Route::post('user/simpan', 'App\Http\Controllers\PenggunaController@simpan')->name('user.simpan');
// Route::post('user/update', 'App\Http\Controllers\PenggunaController@update')->name('user.update');
// Route::get('user/edit/{id}', 'App\Http\Controllers\PenggunaController@edit')->name('user.edit');
// Route::get('user/hapus/{id}', 'App\Http\Controllers\PenggunaController@hapus')->name('user.hapus');

Route::group(['middleware' => 'App\Http\Middleware\ceklogin'], function () {
    Route::get('dashboard', 'App\Http\Controllers\DashboardController@index')->name('dashboard');

    Route::middleware(['App\Http\Middleware\role:isAdmin'])->group(function () {

        Route::get('user', 'App\Http\Controllers\PenggunaController@index')->name('user');
        Route::get('user/tambah', 'App\Http\Controllers\PenggunaController@tambah')->name('user.tambah');
        Route::post('user/simpan', 'App\Http\Controllers\PenggunaController@simpan')->name('user.simpan');
        Route::post('user/update', 'App\Http\Controllers\PenggunaController@update')->name('user.update');
        Route::get('user/edit/{id}', 'App\Http\Controllers\PenggunaController@edit')->name('user.edit');
        Route::get('user/hapus/{id}', 'App\Http\Controllers\PenggunaController@hapus')->name('user.hapus');
    });
});
