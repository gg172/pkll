-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 12, 2023 at 01:53 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pkl`
--

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE `levels` (
  `id` int(11) NOT NULL,
  `urai_level` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`id`, `urai_level`) VALUES
(1, 'admin'),
(2, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `level_user` int(11) NOT NULL,
  `password` text NOT NULL,
  `foto` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `nama`, `username`, `level_user`, `password`, `foto`, `created_at`, `updated_at`) VALUES
(2, 'fahryy', 'fahryy', 1, '$2y$10$Oin9DiqMQ5UKrXNX8msCAeqdz06itAe.1kDPlV7NwIH8GCS0Ohl/S', 'gg.jpg', '2023-01-11 10:45:03', '2023-01-11 17:27:53'),
(3, 'f', 'f', 2, '$2y$10$Y48h5LikYiNIYIbBmVNESejkYJEJiqNJCYccDR137ef5MZ/fMmAri', 'main-qimg-965b11ec95106e64d37f5c380802c305-lq.jfif', '2023-01-11 17:29:44', '2023-01-11 17:38:58'),
(4, 'fsfs', 'fsfs', 1, '$2y$10$UMnLqGVwNDlJcN1fh3brJeIOFDjuZtD7dl6AYbAtJESmsNl50qhiC', 'main-qimg-965b11ec95106e64d37f5c380802c305-lq.jfif', '2023-01-11 17:29:59', '2023-01-11 17:29:59'),
(5, 'fahryy', 'fahryy', 1, '$2y$10$2H6W7jFBBjtv7a0b11WH1uica2C4EuwljyIJn/fjQXdZK6FUuUA5G', 'd97381141ed83f6a68ec84f1ae22f547.jpg', '2023-01-11 17:32:53', '2023-01-11 17:32:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
