<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
</head>

<body>
    <header>
        <a href="#" class="logo">Logo</a>
        <ul>
            <li><a href="#" class="active">home</a></li>
            <li><a href="#">about</a></li>
            <li><a href="#">work</a></li>
            <li><a href="#">contact</a></li>
        </ul>
    </header>
    <section>
        <img src="{{ asset('assets/img/stars.png') }}" id="stars" alt="">
        <img src="{{ asset('assets/img/moon.png') }}" id="moon" alt="">
        <img src="{{ asset('assets/img/mountains_behind.png') }}" id="mountains_behind" alt="">
        <h2 id="text">Moon Light</h2>
        <a href="#sec" id="btn">Explore</a>
        <img src="{{ asset('assets/img/mountains_front.png') }}" id="mountains_front" alt="">
    </section>
    <div class="sec" id="sec">
        <h2>paralax scrolling</h2>
        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Provident odio dolorum accusantium quos at. Eos ducimus laborum ratione quidem aliquam quod cum minus facilis saepe. Minima illum obcaecati error delectus <br>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam reprehenderit tempore saepe accusantium quia quibusdam sapiente laborum maxime molestiae in consectetur placeat debitis, id magnam. Veniam magnam accusantium quia nulla?
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo quo reiciendis libero magni aliquam voluptate consequatur repellat. Soluta impedit consequuntur laborum deleniti blanditiis quo reprehenderit ea odio, nostrum eaque beatae!
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi ipsum, aliquid repudiandae cumque doloribus aliquam temporibus deleniti fugit obcaecati ea vel dolor nobis enim quos eligendi error nemo hic esse. Lorem, ipsum dolor sit amet
            consectetur adipisicing elit. Dolores in laudantium quo maiores quis soluta error. Repellat numquam porro ex nemo nisi nesciunt esse ducimus velit distinctio. Quidem, reprehenderit ab! <br> <br>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Provident odio dolorum accusantium quos at. Eos ducimus laborum ratione quidem aliquam quod cum minus facilis saepe. Minima illum obcaecati error delectus <br>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam reprehenderit tempore saepe accusantium quia quibusdam sapiente laborum maxime molestiae in consectetur placeat debitis, id magnam. Veniam magnam accusantium quia nulla?
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo quo reiciendis libero magni aliquam voluptate consequatur repellat. Soluta impedit consequuntur laborum deleniti blanditiis quo reprehenderit ea odio, nostrum eaque beatae!
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi ipsum, aliquid repudiandae cumque doloribus aliquam temporibus deleniti fugit obcaecati ea vel dolor nobis enim quos eligendi error nemo hic esse. Lorem, ipsum dolor sit amet
            consectetur adipisicing elit. Dolores in laudantium quo maiores quis soluta error. Repellat numquam porro ex nemo nisi nesciunt esse ducimus velit distinctio. Quidem, reprehenderit ab! <br><br>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Provident odio dolorum accusantium quos at. Eos ducimus laborum ratione quidem aliquam quod cum minus facilis saepe. Minima illum obcaecati error delectus <br>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam reprehenderit tempore saepe accusantium quia quibusdam sapiente laborum maxime molestiae in consectetur placeat debitis, id magnam. Veniam magnam accusantium quia nulla?
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo quo reiciendis libero magni aliquam voluptate consequatur repellat. Soluta impedit consequuntur laborum deleniti blanditiis quo reprehenderit ea odio, nostrum eaque beatae!
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi ipsum, aliquid repudiandae cumque doloribus aliquam temporibus deleniti fugit obcaecati ea vel dolor nobis enim quos eligendi error nemo hic esse. Lorem, ipsum dolor sit amet
            consectetur adipisicing elit. Dolores in laudantium quo maiores quis soluta error. Repellat numquam porro ex nemo nisi nesciunt esse ducimus velit distinctio. Quidem, reprehenderit ab! <br> <br>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Provident odio dolorum accusantium quos at. Eos ducimus laborum ratione quidem aliquam quod cum minus facilis saepe. Minima illum obcaecati error delectus <br>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam reprehenderit tempore saepe accusantium quia quibusdam sapiente laborum maxime molestiae in consectetur placeat debitis, id magnam. Veniam magnam accusantium quia nulla?
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo quo reiciendis libero magni aliquam voluptate consequatur repellat. Soluta impedit consequuntur laborum deleniti blanditiis quo reprehenderit ea odio, nostrum eaque beatae!
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi ipsum, aliquid repudiandae cumque doloribus aliquam temporibus deleniti fugit obcaecati ea vel dolor nobis enim quos eligendi error nemo hic esse. Lorem, ipsum dolor sit amet
            consectetur adipisicing elit. Dolores in laudantium quo maiores quis soluta error. Repellat numquam porro ex nemo nisi nesciunt esse ducimus velit distinctio. Quidem, reprehenderit ab!</p>
    </div>
    <script>
        let stars = document.getElementById('stars');
        let moon = document.getElementById('moon');
        let mountains_behind = document.getElementById('mountains_behind');
        let text = document.getElementById('text');
        let btn = document.getElementById('btn');
        let mountains_front = document.getElementById('mountains_front');
        let header = document.querySelector('header');

        window.addEventListener('scroll', function() {
            let value = window.scrollY;
            stars.style.left = value * 0.25 + 'px';
            moon.style.top = value * 0.60 + 'px';
            mountains_behind.style.top = value * 0.50 + 'px';
            mountains_front.style.top = value * 0 + 'px';
            text.style.marginRight = value * 3 + 'px';
            text.style.marginTop = value * 1.5 + 'px';
            btn.style.marginTop = value * 1.5 + 'px';
            header.style.top = value * 0.50 + 'px';
        })
    </script>
</body>

</html>