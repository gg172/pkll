<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GI | User</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
</head>

<body>
    <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Dropdown
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="#">Action</a></li>
                            <li><a class="dropdown-item" href="#">Another action</a></li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li><a class="dropdown-item" href="#">Something else here</a></li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled">Disabled</a>
                    </li>
                </ul>
                <form class="d-flex" role="search">
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </form>
            </div>
        </div>
    </nav>
    <br>
    <br>
    <br>
    <div class="jumbotronn">
        <div class="jumbotron jumbotron-fluid p-1">
            <img class="img-fluid" src="{{ asset('assets/img/bg1.jpg')}}" alt="jumbotronimg">
        </div>
    </div>
    <span class="icono d-flex justify-content-center ">
        <img class="img-fluid" src="{{ asset('assets/img/mouse.png')}}" alt="mouse">
    </span>


    <div class="row" id="home">
        <div class="container-fluid mt-5 text-secondary font-weight-bold" style="max-width: 70%;">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi deserunt eveniet perspiciatis ullam molestiae incidunt, quos voluptatibus soluta, veniam, impedit perferendis fuga aut eius? Debitis dignissimos consequatur rerum sed dolores.</p>
        </div>
    </div>
    <div class="upper m-5" id="kelebihan">
        <h3 class="font-weight-bold text-dark">Kelebihan <b>N</b>godingBareng</h3>
    </div>

    <div class="hahah container d-flex justify-content-center" style="max-height: 30%;">
        <div class="card hh text-center p-5 rounded">
            <img class="img-fluid" src="{{ asset('assets/img/logo.png')}}" alt="logo">
            <h6>Lorem ipsum dolor</h6>
        </div>

        <div class="card hh text-center p-5  rounded">
            <img class="img-fluid" src="{{ asset('assets/img/logo.png')}}" alt="logo">
            <h6>Lorem ipsum dolor sit </h6>
        </div>

        <div class="card hh text-center p-5  rounded">
            <img class="img-fluid" src="{{ asset('assets/img/logo.png')}}" alt="logo">
            <h6>Lorem ipsum dolor sit </h6>
        </div>

        <div class="card hh text-center p-5  rounded">
            <img class="img-fluid" src="{{ asset('assets/img/logo.png')}}" alt="logo">
            <h6>Lorem ipsum dolor sit s</h6>
        </div>
    </div>
    <br>
    <br>
    <!-- fiturutama -->
    <!-- <div class="fitur-utama">
        <h3 class="font-weight-bold text-info text-center">FITUR UTAMA</h3>
    </div> -->

    <!-- manfaat -->

    <div class="container d-flex text-weight-bold text-dark " id="manfaat">
        <div>
            <h2 class="font-weight-bold">Manfaat <b>N</b>godingBareng</h2>
        </div>
    </div>
    <div class="uj d-flex">
        <div class="immg">
            <img class="img-fluid " src="{{asset('assets/img/Homepage_Manfaat-aset-2.png')}}" alt="manfaat">
        </div>

        <div class="table-row text-secondary text-weight-bold ml-5">
            <div class="p1s d-flex" style="height: 5%;">
                <img class="img-fluid ii" src="{{ asset('assets/icon/Homepage_a_kualitas.png')}}" alt="icon">
                <p>Meningkatkan kualitas secara individu</p>
            </div>
            <br>
            <div class="p1s d-flex" style="height: 5%;">
                <img class="img-fluid ii" src="{{ asset('assets/icon/Homepage_b_sarana-informasi.png')}}" alt="icon">
                <p>Sebagai sarana informasi dan hiburan</p>
            </div>
            <br>
            <div class="p1s d-flex" style="height: 5%;">
                <img class="img-fluid ii" src="{{asset('assets/icon/Homepage_c_panduan.png')}}" alt="icon">
                <p>Menjadi panduan dan rujukan resmi</p>
            </div>
            <br>
            <div class="p1s d-flex" style="height: 5%;">
                <img class="img-fluid ii" src="{{asset('assets/icon/Homepage_d_data-insight.png')}}" alt="icon">
                <p>Menjadi data insight bagi institusi</p>
            </div>
            <br>
            <div class="p1s d-flex" style="height: 5%;">
                <img class="img-fluid ii" src="{{asset('assets/icon/Homepage_e_Pembelajaran.png')}}" alt="icon">
                <p>Menjadi medium pembelajaran jarak jauh</p>
            </div>
            <br>
            <div class="p1s d-flex" style="height: 5%;">
                <img class="img-fluid ii" src="{{asset('assets/icon/Homepage_f_universitas.png')}}" alt="icon">
                <p>Meningkatkan akreditasi bagi universitas</p>
            </div>
            <br>
            <div class="p1s d-flex" style="height: 5%;">
                <img class="img-fluid ii" src="{{asset('assets/icon/Homepage_g_smartcity.png')}}" alt="icon">
                <p>Mendukung terciptanya smart city</p>
            </div>
        </div>
    </div>

    <div class="jumbotron jumbotron-fluid bg-secondary mb-5 ">
        <div class="container text-center text-light ">
            <h3 class="">NgodingBareng, Layanan Digital</h3>
            <p class="lead">Nikmati kemudahan coding
                terlengkap dalam satu aplikasi.</p>
            <p> Coba Demo-nya sekarang!</p>
        </div>
    </div>

    <br><br>
    <div class="klien ">
        <h3 class="text-info text-center">Klien</h3>
        <div class="imm ml-5">
            <img class="img-fluid" src="{{asset('assets/img/bca.png')}}" alt="">
            <img class="img-fluid" src="{{asset('assets/img/btpn.png')}}" alt="">
            <img class="img-fluid" src="{{asset('assets/img/dpr.png')}}" alt="">
            <img class="img-fluid" src="{{asset('assets/img/telkomsel.png')}}" alt="">
            <img class="img-fluid" src="{{asset('assets/img/kemendikbud.png')}}" alt="">
            <img class="img-fluid" src="{{asset('assets/img/kemenkeu.png')}}" alt="">
            <img class="img-fluid" src="{{asset('assets/img/logo.png')}}" alt="">
            <img class="img-fluid" src="{{asset('assets/img/bjb.png')}}" alt="">
        </div>
    </div>
    <br>

    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <img class="img-fluid" src="{{asset('assets/img/contact-image.png')}}" alt="contanct">
            <h1 class="display-4 font-weight-bold">
                Kontak Kami</h1>
            <p class="lead">Anda dapat menghubungi Sales (PIC) untuk informasi lebih lanjut mengenai NgodingBareng</p>
        </div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
</body>

</html>